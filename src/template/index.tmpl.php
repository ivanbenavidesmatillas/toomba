<html lang="en">
  <head>
    <title>Toomba test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="description" content="Bloom kitchenbar">
    <meta name="keywords" content="Bloom">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

    <!-- Facebook sharing -->
    <meta property="og:title" content="Bloom kitchenbar">
    <meta property="og:description" content="Bloom kitchenbar ">
    <meta property="og:site_name" content="Bloom kitchenbar">
    <meta property="og:url" content="https://jonthebeach.com/">
    <meta property="og:locale" content="en-EN">
    <meta property="og:image" content="http://jonthebeach.com/application/templates/default/images/logo-share.png?5942d53733198">
    <meta property="og:type" content="website">

    <!-- Twitter sharing -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="Bloom kitchenbar">
    <meta name="twitter:creator" content="Bloom kitchenbar">
    <meta name="twitter:title" content="Bloom kitchenbar">
    <meta name="twitter:description" content="Bloom kitchenbar">
    <meta name="twitter:url" content="https://jonthebeach.com/">
    <meta name="twitter:domain" content="https://jonthebeach.com/">
    <meta name="twitter:image" content="http://jonthebeach.com/application/templates/default/images/logo-share.png?5942d5373324f">


    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

    <link rel="stylesheet" type="text/css" href="/template/css/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/template/css/jquery-ui-1.12.1/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/template/css/style.css" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

  </head>

  <body>
    <div class="content-wrapper">
      <!-- Header -->
      <header>
        <div class="container">
          <a href="/">
            <img src="https://www.toomba.nl/theme/toomba/images/logo.svg" />
          </a>
        </div>
      </header>

      <!-- Main content -->
      <div id="home">
        <div class="container">
          <h1>Bloom kitchenbar booking</h1>
          <section class="booking-form">
            <form class="validate" method="POST" action="/book">
                <div class="row">
                    <div class="col-md-4">
                        <label for="name">First name</label>
                        <input id="name" type="text" name="name" required data-msg="Please enter a name"/>
                    </div>
                    <div class="col-md-4">
                        <label for="last_name">Last name</label>
                        <input id="last_name" type="text" name="last_name" required data-msg="Please enter a last name"/>
                    </div>
                    <div class="col-md-4">
                        <label for="phone">Phone</label>
                        <input id="phone" type="text" name="phone" required data-msg="Please enter valid phone"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="email">Email</label>
                        <input for="id" type="text" name="email" required data-msg-required="Please enter an email" data-msg-email="Please enter a valid email"/>
                    </div>
                    <div class="col-md-4">
                        <label for="date">Date</label>
                        <input id="date" class="date" type="text" name="date" required data-msg="Please enter the reservation date" />
                    </div>
                    <div class="col-md-4">
                        <label for="time">Time</label>
                        <input id="time" class="time" type="text" name="time" required data-msg="Please enter the reservation time" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="amount">Amount of people</label>
                        <input id="amount" class="amount" type="text" name="amount" required data-msg-required="Please enter the amount of people" data-msg-number="Please enter a number" />
                    </div>
                    <div class="col-md-6">
                        <label for="window">Prefers to sit next to a window</label>
                        <input id="window" class="window" type="checkbox" name="window"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                      <footer>
                        <input type="submit" class="button" value="SEND" />
                      </footer>
                    </div>
                </div>
            </form>
          </section>
        </div>
      </div>
      <!-- Footer -->
      <footer>
      </footer>
    </div>

    <script>

        $(window).on('load',function(){
          // Validation
          $('form.validate').each(function(){
              $(this).validate(
              {
                  rules: {
                      email: {
                          required: true,
                          email: true
                      },
                      amount: {
                        number: true
                      }
                  },
                  errorElement : 'div',
              });
          })

          // Date picker
          $( function() {
            $( ".date" ).datepicker();
          } );
        })

        // Clousure
        var fullname = (function () {
            var fullnamevar = '';
            return function () {
              return fullnamevar = $('#name').val() + ' ' + $('#last_name').val() ;
            }
        })();

        $("#name").on('change',function(){
          console.log(fullname());
        });

        $("#last_name").on('change',function(){
          console.log(fullname());
        });

        // Ajax part to check if there is table available
        $(".amount").on('change',function(){
          if(!$(this).hasClass('error')){
            $.ajax({
                type: "GET",
                url: "/checktable.php",
                data: 'amount=' + $(this).val() + 'window=' + ($('.window').prop('checked') ?  '1': '0'),
                async: true,
                success: function(data){
                  var dataObj = eval('(' + data + ')');
                  alert(dataObj.message)
                }
            });
          }
        })
    </script>
  </body>
</html>
